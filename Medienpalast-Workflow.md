# Bojler: Medienpalast-Workflow

1. Windows-Schnellstart deaktivieren

   Um Server, die via WSL 2 laufen, über `localhost` erreichen zu können, muss bei Windows der Schnellstart-Modus deaktiviert werden (mehr Details dazu [hier](https://github.com/microsoft/WSL/issues/5298)).

   Dazu in der Systemsteuerung "System und Sicherheit", "Energieoptionen", "Auswählen, was beim Drücken von Netzschaltern geschehen soll" und "Einige Einstellungen sind momentan nicht verfügbar." wählen und anschließend sicherstellen, dass neben "Schnellstart aktivieren (empfohlen)" kein Haken gesetzt ist.

2. Wechseln des Branches

   Nachdem die Repository geklont wurde, sollte überprüft werden, ob ein Branch für den gewünschten Kunden bereits vorhanden ist oder erst angelegt werden muss (bspw. durch `git branch -r`). Wenn nicht, wird dieser erstellt (`git branch`) – anschließend wird auf jenen Branch gewechselt (`git checkout`).

3. Server einrichten und starten

   Jetzt müssen erst noch die benötigten NPM-Module installiert werden (`npm install`), bevor der Server mit `npm start` gestartet werden kann. Nun kann in **src/templates** gearbeitet werden, während im Browser unter `localhost:8000` eine Vorschau mit LiveReload-Support angezeigt werden kann.

   Achtung: Wird eine Datei unter **src/assets** neu erstellt oder bearbeitet, muss diese erst mit `npm run assets` in den **dist**-Ordner kopiert werden.

   Am besten orientiert man sich bezüglich der Struktur an den vorhandenen HTML-Dateien. Es sollten sofern möglich nur Klassen verwendet und Variablen in der **src/sass/_settings.scss**-Datei angepasst werden. Die Dokumentation zu Bojler findet sich [hier](https://bojler.slicejack.com/documentation/).
